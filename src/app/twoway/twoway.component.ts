import { Component, OnInit } from '@angular/core';

@Component({
  selector: '.app-twoway',
  templateUrl: './twoway.component.html',
  styleUrls: ['./twoway.component.css']
})
export class TwowayComponent implements OnInit {

  username = ''
  constructor() { }

  ngOnInit(): void {
  }

  isUserNameEmpy(): boolean{
    if(this.username=='')
      return true
    else return false
  }

  onResetUserName(event : Event) {
    console.log(event)
    this.username = ''
  }

}
